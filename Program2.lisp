(defun dist-to-rods()
  ;; Take floating point input from the user, asking for a distance in miles
  (princ "Enter a distance in miles. ")
  (setq inp-miles (read))
  ;; Take the input and multiply it by 320
  (setq out-rods (* 320.0 inp-miles))
  ;; Return it to the user
  (format t "~F miles is: ~F" inp-miles out-rods))

(dist-to-rods)
